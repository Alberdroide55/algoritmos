def logger_factory(active=True):
    def wrap(func):
        def wrapper():
            print('Calling ', func.__name__, ' decorator param ', active)
            if active:
                func()
                print('Called ', func.__name__)
            else:
                print('Skipped ', func.__name__)

        return wrapper

    return wrap


logger = logger_factory()
logger_non_active = logger_factory(active=False)


@logger
def func1():
    print('func1')


@logger_non_active
def func2():
    print('func2')


func1()
print('-' * 10)
func2()
