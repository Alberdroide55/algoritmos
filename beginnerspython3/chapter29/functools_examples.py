from functools import lru_cache
import time


# Function that computes fibonacci
# numbers without lru_cache
def fib_without_cache(n):
    if n < 2:
        return n
    return fib_without_cache(n - 1) + fib_without_cache(n - 2)


# Execution start time
begin = time.time()
fib_without_cache(30)
end = time.time()

print("Time taken to execute the\
function without lru_cache is", end - begin)


# Function that computes fibonacci
# numbers with lru_cache
@lru_cache(maxsize=128)
def fib_with_cache(n):
    if n < 2:
        return n
    return fib_with_cache(n - 1) + fib_with_cache(n - 2)


begin = time.time()
fib_with_cache(30)
end = time.time()

print("Time taken to execute the \
function with lru_cache is", end - begin)

# Example taken from https://www.geeksforgeeks.org/python-functools-lru_cache/
