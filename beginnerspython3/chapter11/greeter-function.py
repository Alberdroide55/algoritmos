def greeter(name, message='Live Long and Prosper'):
    print('Welcome', name, '-', message)


greeter('Eloise')
greeter('Eloise', 'Hope you like Rugby')


def greeter(name, title='Dr', prompt='Welcome', message='Live Long and Prosper'):
    print(prompt, title, name, '-', message)


greeter(message='We like Python', name='Lloyd')
greeter('Lloyd', message='We like Python')


def greeter(*names):
    for name in names:
        print('Welcome', name)


greeter('John', 'Denise', 'Phoebe', 'Adam', 'Gryff', 'Jasmine')


def increase_by_one(**kwargs):
    return_dict = {}
    for key, value in kwargs.items():
        return_dict.update({key: value + 1})
    return return_dict


increase_by_one(a=1, b=2, c=3)


def increment(num):
    return num + 1


print(increment(5))
print(increment(5))


def my_sum(*args):
    result = 0
    for x in args:
        result += x
    return result


list1 = [1, 2, 3]
list2 = [4, 5]
list3 = [6, 7, 8, 9]

print(my_sum(*list1, *list2, *list3))


my_first_dict = {"a": 1, "B": 2}
my_second_dict = {"C": 3, "D": 4}
my_merged_dict = {**my_first_dict, **my_second_dict}

print(my_merged_dict)


student_tuples = [
    ('john', 'a', 15),
    ('jane', 'B', 12),
    ('dave', 'B', 10),
]

sorted(student_tuples, key=lambda student: student[2])