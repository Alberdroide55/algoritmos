class Person:
    """ An example class to hold a persons name and age"""

    instance_count = 0

    @classmethod
    def increment_instance_count(cls):
        cls.instance_count += 1

    @staticmethod
    def static_function():
        print('Static method')

    def __init__(self, name, age):
        Person.increment_instance_count()
        self.name = name
        self.age = age


Person.static_function()
