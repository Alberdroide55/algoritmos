class Person:
    def __init__(self, name: str):
        self._name = name
        self._age = None

    @property
    def age(self):
        """ The docstring for the age property """
        print('In age method')
        return self._age

    @age.setter
    def age(self, value):
        print('In set_age method')
        if isinstance(value, int) & value > 0 & value < 120:
            self._age = value


person = Person('John')
print(person.age)
person.age = 21
print(person.age)
person.age = -2
print(person.age)
