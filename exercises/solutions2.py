import collections
# Ejercicio 1
# Jenny has written a function that returns a greeting for a user. However, she's in love with Johnny, and would like to greet him slightly different. She added a special case to her function, but she made a mistake.
# He escrito un codigo que devuelve un saludo al usuario, sin embargo quiero darle un saludo especial
# a mi mejor amigo Juan Jose. He creado esta funcion pero he cometido un fallo. Podemos arreglarlo?


def greet(name):
    if name == "Johnny":
        return "Hello, my love!"
    return f"Hello, {name}!"


def greet2(name):
    message = f"Hello, {name}!"
    if name == "Johnny":
        message = "Hello, my love!"
    return message


def greet3(name):
    return "Hello, my love!" if name == "Johnny" else f"Hello, {name}!"

# Crear una funcion que reciba un parametro name como entrada de la función y me devuelva
# name + " toca la guitarra!" o name + " no toca la guitarra..." siempre que el nombre empiece por "R" o "r".


def are_you_playing_guitar(name: str) -> str:
    return name + " toca la guitarra!" if name.startswith(__prefix=("R", "r")) else name + " no toca la guitarra..."

# Crear una función que cuente las ovejas que hay en un array. True significa que está la oveja y False significa que
# no esta presente.
# Por ejemplo [True, True, False] -> 3


def count_sheeps(sheep):
    count = 0
    for b in sheep:
        if b:
            count += 1
    return count


def count_sheeps2(sheep):
    return len([x for x in sheep if x])


def count_sheeps3(array_of_sheep):
    return array_of_sheep.count(True)

# Given a random non-negative number, you have to return the digits of this number within an array in reverse order.
# Dado un numero POSITIVO > 0, crear una funcion que devuelva los digitos de sus numeros en order inverso.
# 35231 => [1,3,2,5,3]
# 0 => [0]
# 984764738 => [8,3,7,4,6,7,4,8,9])


def digitize(n):
    return map(int, reversed(str(n)))


def digitize2(n):
    return [int(x) for x in str(n)[::-1]]


def digitize3(n):
    return map(int, str(n)[::-1])


def digitize4(n):
    queue = collections.deque()
    for number in str(n):
        queue.appendleft(int(number))
    return list(queue)

# Crear un programa que encuentre la 'aguja' en una lista de objetos.
# ["Hay", "una", 3, [48,34], "mas cosas", "aguja", "en", "un", "pajar"] --> "Encontrada la aguja en la posición: 5"


def find_needle(lista):
    i = None
    for index in range(len(lista)):
        if str(lista[index]) == "needle":
            i = index
    return f"Encontrada la aguja en la posición: {i}"


# Write a function that returns the total surface area and volume of a box as an array: [area, volume]
# Crea una function que deuelva una tupla de (area, volumen) de una caja dados w,h,l:
# l es el largo de la caja, o el lado más largo.
# h es la altura de la caja.
# w es el ancho de la caja.

def get_size(w, h, l):
    area = 2*l*w + 2*l*h + 2*h*w
    volume = l*w*h
    return area, volume


def get_size2(w, h, l):
    return [2 * (w*l + h*l + h*w), w*h*l]

# piedra papel tijera
# Piedra papel o tijera con dos jugadores.
# Tiene que devolver "Draw!" o "Player 1 won!" o "Player 2 won!"


def ppt(p1, p2):
    if p1 == p2:
        return "Draw!"
    elif p1 == "piedra":
        if p2 == "tijera":
            return "Player 1 won!"
        else:
            return "Player 2 won!"
    elif p1 == "papel":
        if p2 == "piedra":
            return "Player 1 won!"
        else:
            return "Player 2 won!"
    elif p1 == "tijera":
        if p2 == "papel":
            return "Player 1 won!"
        else:
            return "Player 2 won!"


def ppt2(p1, p2):
    beats = {'rock': 'scissors', 'scissors': 'paper', 'paper': 'rock'}
    if beats[p1] == p2:
        return "Player 1 won!"
    if beats[p2] == p1:
        return "Player 2 won!"
    return "Draw!"

